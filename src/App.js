import { Route, Routes } from "react-router-dom";
import Admin from "./Components/Admin/Admin";
import Kassa from "./Components/Kassa/Kassa";
import SignIn from "./Components/Login/SignIn";
import Sales, {
  IncomeProductList,
  OutcomeProductList,
  TodayProductList,
} from "./Components/Sales/Sales";
import WareHouse from "./Components/WareHouse/WareHouse";
import "../src/Components/Style/main.css";
import Settings from "./Components/SideBar/Settings";
import User from "./Components/Admin/User";
import SuperAdmin from "./Components/SuperAdmin/SuperAdmin";
import MarketList from "./Components/Admin/MarketList";
import CompanyList from "./Components/Admin/CompanyList";

function App() {
  return (
    <Routes>
      <Route path="/" element={<SignIn />} />
      <Route path="/admin" element={<Admin />} />
      <Route path="/cashier" element={<Kassa />} />
      <Route path="/sales" element={<Sales />} />
      <Route path="/sales/today" element={<TodayProductList />} />
      <Route path="/sales/outcome" element={<OutcomeProductList />} />
      <Route path="/sales/income" element={<IncomeProductList />} />
      <Route path="/wareHouse" element={<WareHouse />} />
      <Route path="/setting" element={<Settings />} />
      <Route path="/admin/userList" element={<User />} />
      <Route path="/admin/marketList" element={<MarketList />} />
      <Route path="/superAdmin" element={<SuperAdmin />} />
      <Route path="/companyList" element={<CompanyList />} />
    </Routes>
  );
}

export default App;
