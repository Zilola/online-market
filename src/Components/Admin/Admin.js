import React, { useEffect, useState } from "react";
import SideBar from "../SideBar/SideBar";
import AddOrganization from "../SuperAdmin/AddOrganization";
import UpdateOrganization from "../SuperAdmin/UpdateOrganization";
import AddMarket from "./AddMarket";
import AddUser from "./AddUser";
import AddWarehouse from "./AddWarehouse";
import MarketList from "./MarketList";

export default function Admin() {
  const photo =
    "https://img.freepik.com/free-vector/businessman-character-avatar-isolated_24877-60111.jpg?w=2000";
  const data = [
    {
      name: "Umar",
      fullname: "Tashkent. Chorse -123",

      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      role: "cashier",
      contact: "+998 99 999 99 98",
    },
    {
      name: "Umarjon",
      fullname: "Adress",

      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      role: "seller",
      contact: "+998 99 999 99 97",
    },
    {
      name: "Umartoy",
      fullname: "Adress",

      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      role: "warehouseman",
      contact: "+998 99 999 99 94",
    },
  ];

  // const [inputSearch, setinputSearch] = useState("");
  const [filterData, setfilterData] = useState(data);
  const [update, setUpdate] = useState(true);

  // const onChangeHand = (e) => {
  //   setinputSearch(e.target.value);
  // };

  const addStaff = (objStaff) => {
    setfilterData(() => [...filterData, objStaff]);
    setUpdate(!update);
  };

  // useEffect(() => {
  //   console.log(data);
  //   const searchedData = data.filter((el) => {
  //     return (
  //       el.name.toLocaleLowerCase().includes(inputSearch.toLocaleLowerCase()) ||
  //       el.contact
  //         .toLocaleLowerCase()
  //         .includes(inputSearch.toLocaleLowerCase()) ||
  //       el.role.toLocaleLowerCase().includes(inputSearch.toLocaleLowerCase())
  //     );
  //   });

  //   setfilterData(searchedData);
  // }, [inputSearch]);

  return (
    <>
      <SideBar />
      <div className="container pt-5">
        <div className="col-md-8 col-sm-12 offset-md-2 offset-sm-0 ">
          <div class="row">
            <div class="col-md-4 position-reletive">
              <div class="card bg-gradient-danger card-img-holder text-white border-0">
                <div class="card-body" style={{ overflow: "none" }}>
                  <img
                    src="https://www.bootstrapdash.com/demo/purple-react-free/template/demo_1/preview/static/media/circle.1541da91.svg"
                    class="position-absolute  h-100"
                    alt="circle-image"
                    style={{ top: "0", right: "0" }}
                  />
                  <h4 class="font-weight-normal mb-3">
                    Daily income
                    <i class="mdi mdi-diamond mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5">95,5741</h2>
                  <h6 class="card-text"></h6>
                </div>
              </div>
            </div>
            <div class="col-md-4 position-reletive">
              <div class="card bg-gradient-info card-img-holder text-white border-0">
                <div class="card-body" style={{ overflow: "none" }}>
                  <img
                    src="https://www.bootstrapdash.com/demo/purple-react-free/template/demo_1/preview/static/media/circle.1541da91.svg"
                    class="position-absolute  h-100"
                    alt="circle-image"
                    style={{ top: "0", right: "0" }}
                  />
                  <h4 class="font-weight-normal mb-3">
                    Monthly income{" "}
                    <i class="mdi mdi-diamond mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5">95,5741</h2>
                </div>
              </div>
            </div>
            <div class="col-md-4 position-reletive">
              <div class="card bg-gradient-success card-img-holder text-white border-0">
                <div class="card-body" style={{ overflow: "none" }}>
                  <img
                    src="https://www.bootstrapdash.com/demo/purple-react-free/template/demo_1/preview/static/media/circle.1541da91.svg"
                    class="position-absolute  h-100"
                    alt="circle-image"
                    style={{ top: "0", right: "0" }}
                  />
                  <h4 class="font-weight-normal mb-3">
                    Number of customers
                    <i class="mdi mdi-diamond mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5">1000</h2>
                </div>
              </div>
            </div>
          </div>
          <div className="row my-4">
            <div className="col-3">
              <button
                className="btn btncolor w-100"
                data-bs-toggle="modal"
                data-bs-target="#exampleModal"
              >
                Add new market
              </button>
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <button
                className="btn btncolor w-100"
                data-bs-toggle="modal"
                data-bs-target="#exampleModal"
              >
                Add new warehouse
              </button>
            </div>
          </div>
          <div className="row">
            <div className="col-12"></div>
          </div>
        </div>
      </div>
      <div
        class="modal fade  "
        style={{ width: "100%" }}
        id="exampleModal"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog ">
          <div class="modal-content p-3">
            <div class="modal-header">
              <button
                type="button"
                class="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={() => setUpdate(false)}
              ></button>
            </div>
            <div class="modal-body">
              {update ? <AddMarket /> : <AddWarehouse onAddFunc={addStaff} />}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
