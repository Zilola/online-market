import React, { useEffect, useState } from "react";
import SideBar from "../SideBar/SideBar";
import AddUser from "./AddUser";

export default function User() {
  const photo =
    "https://img.freepik.com/free-vector/businessman-character-avatar-isolated_24877-60111.jpg?w=2000";
  const data = [
    {
      name: "Umar",
      fullname: "Ikramova",

      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      role: "cashier",
      contact: "+998 99 999 99 98",
    },
    {
      name: "Umarjon",
      fullname: "Ikramova",

      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      role: "seller",
      contact: "+998 99 999 99 97",
    },
    {
      name: "Umartoy",
      fullname: "Ikramova",

      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      role: "warehouseman",
      contact: "+998 99 999 99 94",
    },
  ];
  const [allData, setAllData] = useState(data);
  const [inputSearch, setinputSearch] = useState("");
  const [filterData, setfilterData] = useState(data);

  const del = (id) => {
    const x = filterData.filter((el, i) => {
      if (i !== id) return el;
    });
    setfilterData(x);
  };
  const onChangeHand = (e) => {
    setinputSearch(e.target.value);
  };

  const addStaff = (objStaff) => {
    setAllData((prev) => [...prev, objStaff]);
    setfilterData((prev) => [...prev, objStaff]);
  };

  useEffect(() => {
    console.log(data);
    const searchedData = allData.filter((el) => {
      return (
        el.name.toLocaleLowerCase().includes(inputSearch.toLocaleLowerCase()) ||
        el.contact
          .toLocaleLowerCase()
          .includes(inputSearch.toLocaleLowerCase()) ||
        el.role.toLocaleLowerCase().includes(inputSearch.toLocaleLowerCase())
      );
    });

    setfilterData(searchedData);
  }, [inputSearch]);

  console.log(filterData);
  return (
    <>
      <SideBar />
      <div className="container pt-5">
        <div className="col-md-8 col-sm-12 offset-md-2 offset-sm-0 ">
          <h2 className="text-center mt-3 ">
            User <b>Management</b>
          </h2>
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Recipient's username"
              onChange={onChangeHand}
              value={inputSearch}
            />
            <a href="#" className="text-white">
              <button
                className="btn btncolor"
                data-bs-toggle="modal"
                data-bs-target="#exampleModal"
              >
                Add New User
              </button>
            </a>
          </div>

          <div className="mt-4 contentBg p-3">
            <table className="table  table-hover ">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Fullname</th>
                  <th>Role</th>
                  <th>Contact</th>
                  <th>Active</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                {filterData.map((el, i) => {
                  return (
                    <tr style={{ verticalAlign: "middle" }} key={i}>
                      <td>{i + 1}</td>
                      <td>
                        <img
                          src={el.photo ? el.photo : photo}
                          width="80px"
                          height="80px"
                          style={{ borderRadius: "50%" }}
                          className="me-3"
                        />
                        {el.name}
                      </td>
                      <td>{el.fullname}</td>
                      <td>{el.role}</td>
                      <td>{el.contact}</td>
                      <td>
                        {" "}
                        <div className="switch ms-4">
                          <input
                            id={i}
                            type="checkbox"
                            className="switch-input ms-1"
                          />
                          <label for={i} className="switch-label ">
                            Switch
                          </label>
                        </div>
                      </td>
                      <td>
                        <button
                          className="btn btn-outline-danger w-100"
                          onClick={() => del(i)}
                        >
                          Dalete
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
        <div
          class="modal fade  "
          style={{ width: "100%" }}
          id="exampleModal"
          tabindex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div class="modal-dialog ">
            <div class="modal-content p-3">
              <div class="modal-header">
                <button
                  type="button"
                  class="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div class="modal-body">
                <AddUser onAddFunc={addStaff} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
