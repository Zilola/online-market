import React, { useState } from "react";

export default function AddMarket(props) {
  const [username, setUserName] = useState("");
  const [fullname, setFullName] = useState("");
  const [contact, setContact] = useState("");
  const [role, setRole] = useState("");
  const [pass, setpass] = useState("");

  const onChangeHand = (e) => {
    switch (e.target.name) {
      case "name":
        setUserName(e.target.value);
        console.log(username);
        console.log(e.target.value);
        break;
      case "password":
        setpass(e.target.value);
        break;
      case "fullname":
        setFullName(e.target.value);
        break;
      case "contact":
        setContact(e.target.value);
        break;
      case "exampleRadios":
        setRole(e.target.value);
        break;
    }
  };
  const saveStaff = () => {
    const newStaff = {
      name: username,
      fullname: fullname,
      contact: contact,
      pass: pass,
      role: role,
    };
    props.onAddFunc(newStaff);

    setUserName("");
    setFullName("");
    setContact("");
    setRole("");
    setpass("");
  };
  return (
    <div className="container">
      <div className="row">
        <div className="col-12  ">
          <h1 className="text-center ">Add staff</h1>
          <input
            value={username}
            onChange={onChangeHand}
            type="text"
            placeholder="Enter username..."
            required
            className="form-control mt-2"
            name="name"
          />
          <input
            value={fullname}
            onChange={onChangeHand}
            name="fullname"
            type="text"
            placeholder="Enter fullname..."
            required
            className="form-control mt-2"
          />
          <input
            value={contact}
            onChange={onChangeHand}
            name="contact"
            type="text"
            placeholder="Enter phone..."
            required
            className="form-control mt-2"
          />
          <input
            value={pass}
            onChange={onChangeHand}
            name="password"
            type="text"
            placeholder="Enter password..."
            required
            className="form-control mt-2"
          />
          <div class="form-check mt-2">
            <input
              checked={role === "" ? false : true}
              onChange={onChangeHand}
              class="form-check-input"
              type="radio"
              name="exampleRadios"
              id="exampleRadios1"
              value="Seller"
            />
            <label class="form-check-label" for="exampleRadios1">
              Seller
            </label>
          </div>
          <div class="form-check">
            <input
              checked={role === "" ? false : true}
              onChange={onChangeHand}
              class="form-check-input"
              type="radio"
              name="exampleRadios"
              id="exampleRadios2"
              value="Cashier"
            />
            <label class="form-check-label" for="exampleRadios2">
              Cashier
            </label>
          </div>
          <div class="form-check disabled">
            <input
              checked={role === "" ? false : true}
              onChange={onChangeHand}
              class="form-check-input"
              type="radio"
              name="exampleRadios"
              id="exampleRadios3"
              value="Warehouseman"
            />
            <label class="form-check-label" for="exampleRadios3">
              Warehouseman
            </label>
          </div>

          <button
            type="button"
            class="btn text-white btncolor  w-50 "
            style={{ margin: "10% 25%" }}
            data-bs-dismiss="modal"
            onClick={saveStaff}
          >
            Save
          </button>
        </div>
      </div>
    </div>
  );
}
