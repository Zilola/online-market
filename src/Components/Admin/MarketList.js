import React, { useEffect, useState } from "react";
import SideBar from "../SideBar/SideBar";
import AddMarket from "./AddMarket";
import AddUser from "./AddUser";

export default function MarketList() {
  const photo =
    "https://img.freepik.com/free-vector/businessman-character-avatar-isolated_24877-60111.jpg?w=2000";
  const data = [
    {
      name: "Umar",
      fullname: "Tashkent. Chorse -123",

      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      role: "cashier",
      contact: "+998 99 999 99 98",
    },
    {
      name: "Umarjon",
      fullname: "Adress",

      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      role: "seller",
      contact: "+998 99 999 99 97",
    },
    {
      name: "Umartoy",
      fullname: "Adress",

      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      role: "warehouseman",
      contact: "+998 99 999 99 94",
    },
  ];

  const [inputSearch, setinputSearch] = useState("");
  const [filterData, setfilterData] = useState(data);

  const onChangeHand = (e) => {
    setinputSearch(e.target.value);
  };

  const addStaff = (objStaff) => {
    setfilterData(() => [...filterData, objStaff]);
  };

  useEffect(() => {
    console.log(data);
    const searchedData = data.filter((el) => {
      return (
        el.name.toLocaleLowerCase().includes(inputSearch.toLocaleLowerCase()) ||
        el.contact
          .toLocaleLowerCase()
          .includes(inputSearch.toLocaleLowerCase()) ||
        el.role.toLocaleLowerCase().includes(inputSearch.toLocaleLowerCase())
      );
    });

    setfilterData(searchedData);
  }, [inputSearch]);

  return (
    <>
      <SideBar />
      <div className="container pt-5">
        <div className="col-md-8 col-sm-12 offset-md-2 offset-sm-0 ">
          <h2 className="text-center mt-3 ">
            Market <b>Management</b>
          </h2>
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Recipient's username"
              onChange={onChangeHand}
              value={inputSearch}
            />
            <a href="#" className="text-white">
              <button
                className="btn btncolor"
                data-bs-toggle="modal"
                data-bs-target="#exampleModal"
              >
                Add new market
              </button>
            </a>
          </div>

          <div className="mt-4 contentBg p-3">
            <table className="table  table-hover ">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Market name</th>
                  <th colspan="2">Location</th>
                  <th>Describe</th>
                  
                </tr>
              </thead>
              <tbody>
                {filterData.map((el, i) => {
                  return (
                    <tr style={{ verticalAlign: "middle" }} key={i}>
                      <td>{i + 1}</td>
                      <td>{el.name}</td>
                      <td colspan="2">{el.fullname}</td>
                      <td>{el.contact}</td>
                    
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
        <div
          class="modal fade  "
          style={{ width: "100%" }}
          id="exampleModal"
          tabindex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div class="modal-dialog ">
            <div class="modal-content p-3">
              <div class="modal-header">
                <button
                  type="button"
                  class="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div class="modal-body">
                <AddMarket onAddFunc={addStaff} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
