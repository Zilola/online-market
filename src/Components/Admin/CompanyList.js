import React, { useEffect, useState } from "react";
import SideBar from "../SideBar/SideBar";
import AddCompany from "./AddCompany";
import AddNewFirm from "./AddNewFirm";

export default function CompanyList() {
  const photo =
    "https://img.freepik.com/free-vector/businessman-character-avatar-isolated_24877-60111.jpg?w=2000";

  const data = [
    {
      name: "Umar",
      fir: ["bir", "ikki", "uch"],
    },
    {
      name: "Umarjon",
      fir: ["bir", "ikki", "uh"],
    },
    {
      name: "Umartoy",
      fir: ["bir", "ikk", "uch"],
    },
  ];

  const [allData, setAllData] = useState(data);
  const [inputSearch, setinputSearch] = useState("");
  const [filterData, setfilterData] = useState(data);
  const [newFirmBtn, setNewFirmBtn] = useState(false);
  const [newFirmId, setNewFirmId] = useState();

  const del = (id) => {
    const x = filterData.filter((el, i) => {
      if (i !== id) return el;
    });
    setfilterData(x);
  };
  const onChangeHand = (e) => {
    setinputSearch(e.target.value);
  };

  // add company
  const addStaff = (objStaff) => {
    setAllData((prev) => [...prev, objStaff]);
    setfilterData(allData);
  };
  const addFirmModal = (objStaff) => {
    setAllData(() => {
      allData[newFirmId].fir.push(objStaff);
    });
    setfilterData(allData);
  };

  //   Add firma
  const addFirm = (i) => {
    setNewFirmId(i);
    setNewFirmBtn(true);
  };

  useEffect(() => {
    const searchedData = allData.filter((el) => {
      return el.name
        .toLocaleLowerCase()
        .includes(inputSearch.toLocaleLowerCase());
    });

    setfilterData(searchedData);
  }, [inputSearch]);

  return (
    <>
      <SideBar />
      <div className="container pt-5">
        <div className="col-md-8 col-sm-12 offset-md-2 offset-sm-0 ">
          <h2 className="text-center mt-3 ">
            Company <b>Management</b>
          </h2>
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Recipient's username"
              onChange={onChangeHand}
              value={inputSearch}
            />
            <a href="#" className="text-white">
              <button
                onClick={() => setNewFirmBtn(false)}
                className="btn btncolor"
                data-bs-toggle="modal"
                data-bs-target="#exampleModal"
              >
                Add New Company
              </button>
            </a>
          </div>

          <div className="mt-4 contentBg p-3">
            <table className="table  table-hover ">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Firm</th>
                  <th>Active</th>
                  <th>Add Firm</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                {filterData.map((el, i) => {
                  return (
                    <tr style={{ verticalAlign: "middle" }} key={i}>
                      <td>{i + 1}</td>
                      <td>{el.name}</td>
                      <td>
                        <select class="form-select">
                          {el.fir.map((item) => {
                            return <option value={item}>{item}</option>;
                          })}
                        </select>
                      </td>

                      <td>
                        <div className="switch ms-4">
                          <input
                            id={i}
                            type="checkbox"
                            className="switch-input ms-1"
                          />
                          <label for={i} className="switch-label ">
                            Switch
                          </label>
                        </div>
                      </td>
                      <td>
                        <button
                          className="btn btn-outline-success w-100"
                          onClick={() => addFirm(i)}
                          data-bs-toggle="modal"
                          data-bs-target="#exampleModal"
                        >
                          Add Firm
                        </button>
                      </td>
                      <td>
                        <button
                          className="btn btn-outline-danger w-100"
                          onClick={() => del(i)}
                        >
                          Dalete
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
        <div
          class="modal fade  "
          style={{ width: "100%" }}
          id="exampleModal"
          tabindex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div class="modal-dialog ">
            <div class="modal-content p-3">
              <div class="modal-header">
                <button
                  type="button"
                  class="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div class="modal-body">
                {newFirmBtn ? (
                  <AddNewFirm onAddFunc={addFirmModal} />
                ) : (
                  <AddCompany onAddFunc={addStaff} />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
