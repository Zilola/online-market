import React, { useState } from "react";

export default function AddNewFirm(props) {
  const [username, setUserName] = useState("");

  const onChangeHand = (e) => {
    setUserName(e.target.value);
  };

  const saveStaff = () => {
    const newStaff = username;

    props.onAddFunc(newStaff);

    setUserName("");
  };
  return (
    <div className="container">
      <div className="row">
        <div className="col-12  ">
          <h1 className="text-center ">Add Firm</h1>
          <input
            value={username}
            onChange={onChangeHand}
            type="text"
            placeholder="Enter username..."
            required
            className="form-control mt-2"
            name="name"
          />

          <button
            type="button"
            class="btn text-white btncolor  w-50 "
            style={{ margin: "10% 25%" }}
            data-bs-dismiss="modal"
            onClick={saveStaff}
          >
            Save
          </button>
        </div>
      </div>
    </div>
  );
}
