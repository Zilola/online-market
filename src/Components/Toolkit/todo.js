import { createSlice } from "@reduxjs/toolkit";
import { v4 } from "uuid";
import { useSelector, useDispatch } from "react-redux";

const initialState = {
  value: [],
};

export const todoSlice = createSlice({
  name: "todo",
  initialState,

  reducers: {
    updataOrg: (state, action) => {
      // console.log(action);

      state.value.map((item) => {
        
        item.contact = action.payload.name;
        item.name = action.payload.name;
        item.email = action.payload.email;
        item.pass = action.payload.password;
        item.fullname = action.payload.fullname;
        item.location = action.payload.location;;
       
        // item.id=undefined;
      });
    },
  },
});

export const { updataOrg } = todoSlice.actions;

export default todoSlice.reducer;
