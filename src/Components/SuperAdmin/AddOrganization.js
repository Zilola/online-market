import React, { useState } from 'react'

export default function AddOrganization(props) {
   const [username, setUserName] = useState("");
   const [fullname, setFullName] = useState("");
   const [contact, setContact] = useState("");
   const [location, setLocation] = useState("");
   const [pass, setpass] = useState("");
      const [gmail, setGmail] = useState("");


   const onChangeHand = (e) => {
     switch (e.target.name) {
       case "name":
         setUserName(e.target.value);
         console.log(username);
         console.log(e.target.value);
         break;
       case "password":
         setpass(e.target.value);
         break;
       case "fullname":
         setFullName(e.target.value);
         break;
       case "contact":
         setContact(e.target.value);
         break;
       case "location":
         setLocation(e.target.value);
         break;
       case "gmail":
         setGmail(e.target.value);
         break;
     }
   };
   const saveStaff = () => {
     const newStaff = {
       name: username,
       fullname: fullname,
       contact: contact,
       pass: pass,
       location: location,
     };
     props.onAddFunc(newStaff);

     setUserName("");
     setFullName("");
     setContact("");
     setLocation("");
     setpass("");
     setGmail("")
   };
   return (
     <div className="container">
       <div className="row">
         <div className="col-12  ">
           <h1 className="text-center ">Add Organization</h1>
           <input
             value={username}
             onChange={onChangeHand}
             type="text"
             placeholder="Enter username..."
             required
             className="form-control mt-2"
             name="name"
           />
           <input
             value={fullname}
             onChange={onChangeHand}
             name="fullname"
             type="text"
             placeholder="Enter fullname..."
             required
             className="form-control mt-2"
           />
           <input
             value={contact}
             onChange={onChangeHand}
             name="contact"
             type="text"
             placeholder="Enter phone..."
             required
             className="form-control mt-2"
           />
           <input
             value={pass}
             onChange={onChangeHand}
             name="password"
             type="text"
             placeholder="Enter password..."
             required
             className="form-control mt-2"
           />
           <input
             value={location}
             onChange={onChangeHand}
             name="location"
             type="text"
             placeholder="Enter location..."
             required
             className="form-control mt-2"
           />
           <input
             value={gmail}
             onChange={onChangeHand}
             name="gmail"
             type="text"
             placeholder="Enter email..."
             required
             className="form-control mt-2"
           />

           <button
             type="button"
             class="btn text-white btncolor  w-50 "
             style={{ margin: "10% 25%" }}
             data-bs-dismiss="modal"
             onClick={saveStaff}
           >
             Save
           </button>
         </div>
       </div>
     </div>
   );
}
