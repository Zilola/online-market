import React, { useState } from "react";

export default function AddAdmin(props) {

  const [username, setUserName] = useState("");
  const [pass, setpass] = useState("");

  const onChangeHand = (e) => {
    switch (e.target.name) {
      case "name":
        setUserName(e.target.value);
        console.log(username);
        console.log(e.target.value);
        break;
      case "password":
        setpass(e.target.value);
        break;
    }
  };


  const saveStaff = () => {
    const newStaff = {
      name: username,
      pass: pass,
    };
    // props.onAddFunc(newStaff);

    setUserName("");
    setpass("");
   
  };
  return (
    <div className="container">
      <div className="row">
        <div className="col-12  ">
          <h1 className="text-center ">Add Admin</h1>
          <input
            value={username}
            onChange={onChangeHand}
            type="text"
            placeholder="Enter Username..."
            required
            className="form-control mt-2"
            name="name"
          />
         
          <input
            value={pass}
            onChange={onChangeHand}
            name="password"
            type="text"
            placeholder="Enter password..."
            required
            className="form-control mt-2"
          />
         
          <button
            type="button"
            class="btn text-white btncolor  w-50 "
            style={{ margin: "10% 25%" }}
            data-bs-dismiss="modal"
            onClick={saveStaff}
          >
            Save
          </button>
        </div>
      </div>
    </div>
  );
}
