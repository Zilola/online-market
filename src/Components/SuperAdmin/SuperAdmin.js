import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import AddUser from "../Admin/AddUser";
import SideBar from "../SideBar/SideBar";
import { updataOrg } from "../Toolkit/todo";
import AddAdmin from "./AddAdmin";
import AddOrganization from "./AddOrganization";
import UpdateOrganization from "./UpdateOrganization";

export default function SuperAdmin() {
  const data = [
    {
      name: "Umar",
      fullname: "Ikramova",
      gmail: "Folonchiyev.pismadonchi@gmail.com",
      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      adress: " Tashkent, Yunusobod bozori 123 dokoni",
      contact: "+998 99 999 99 98",
    },
    {
      name: "Umar",
      fullname: "Ikramova",
      gmail: "Folonchiyev.pismadonchi@gmail.com",
      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      adress: " Tashkent, Yunusobod bozori 123 dokoni",
      contact: "+998 99 999 99 98",
    },
    {
      name: "Umartoy",
      fullname: "Ikramova",
      gmail: "Folonchiye.pismadonchi@gmail.com",
      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      adress: " Tashkent, Yunusobod bozori 125 dokoni",
      contact: "+998 99 999 99 90",
    },
    {
      name: "Umarjon",
      fullname: "Ikramova",
      gmail: "Folonchiyeva.pismadonchi@gmail.com",
      photo:
        "https://target.scene7.com/is/image/Target/Kids-220217-1645116370369",
      adress: " Tashkenta, Yunusobod bozori 123 dokoni",
      contact: "+998 99 999 99 985",
    },
  ];
  const dispatch = useDispatch();
  const [inputSearch, setinputSearch] = useState("");
  const [filterData, setfilterData] = useState(data);
  const [btn, setBtn] = useState("");

  const element = [];

  const onChangeHand = (e) => {
    setinputSearch(e.target.value);
  };

  //add Org

  const addOrg = () => {
    setBtn("addOrg");
  };

  //save org

  const addStaff = (objStaff) => {
    setfilterData(() => [...filterData, objStaff]);
    data.push(objStaff);
    setBtn("");
  };
  //Del Org
  const del = (id) => {
    const x = filterData.filter((el, i) => {
      if (i !== id) return el;
    });
    setfilterData(x);
  };

  //update org

  const updateBtn = (i, el) => {
    setBtn("update");

    dispatch(
      updataOrg({
        name: el.name,
        fullname: el.fullname,
        contact: el.contact,
        pass: el.pass,
        location: el.location,
      })
    );
  };

  const updateOrg = (obj, test) => {
    console.log(obj);
    if (test) {
      setBtn("update");
    }
    element = [];
  };

  //Add admin
  const addAdmin = (id) => {
    setBtn("add");
  };

  useEffect(() => {
    const searchedData = data.filter((el) => {
      return (
        el.name.toLocaleLowerCase().includes(inputSearch.toLocaleLowerCase()) ||
        el.contact
          .toLocaleLowerCase()
          .includes(inputSearch.toLocaleLowerCase()) ||
        el.fullname
          .toLocaleLowerCase()
          .includes(inputSearch.toLocaleLowerCase()) ||
        el.adress
          .toLocaleLowerCase()
          .includes(inputSearch.toLocaleLowerCase()) ||
        el.gmail.toLocaleLowerCase().includes(inputSearch.toLocaleLowerCase())
      );
      setBtn("");
    });

    setfilterData(searchedData);
  }, [inputSearch]);

  console.log(btn);
  return (
    <>
      <SideBar />
      <div className="container pb-5">
        <div className="row">
          <div className="col-8 offset-2">
            <div className="row ">
              <h2 className="text-center mt-5 ">
                Organization <b>Management</b>
              </h2>
              <div className="input-group mb-3 p-0">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Recipient's username"
                  onChange={onChangeHand}
                  value={inputSearch}
                />
                <a href="#" className="text-white">
                  <button
                    className="btn btncolor"
                    data-bs-toggle="modal"
                    data-bs-target="#exampleModal"
                    onClick={addOrg}
                  >
                    Add New Organization
                  </button>
                </a>
              </div>
            </div>

            {filterData.map((el, i) => {
              return (
                <div className="row contentBg border-box p-3 mb-2" key={i}>
                  <div className="col-4  d-flex align-items-center">
                    <img
                      src="https://www.logodee.com/wp-content/uploads/2020/07/LD-C-46.jpg"
                      width="80%"
                    />
                  </div>
                  <div className="col-6 p-1 ">
                    <p className="d-flex align-items-center">
                      <i className="bx bx-user nav_link m-0 me-1 p-0"></i>
                      {el?.name + " " + el?.fullname}
                    </p>
                    <p className="d-flex align-items-center">
                      <i className="bx bx-navigation nav_link m-0 me-1 p-0"></i>
                      {el?.adress}
                    </p>
                    <p className="d-flex align-items-center">
                      <i className="bx bx-message nav_link m-0 me-1 p-0"></i>
                      {el?.gmail}
                    </p>
                    <p className="d-flex align-items-center">
                      <i className="bx bx-phone nav_link m-0 me-1 p-0"></i>
                      {el?.contact}
                    </p>
                  </div>
                  <div className="col-2 p-2">
                    <button
                      className="btn btn-outline-danger w-100"
                      onClick={() => del(i)}
                    >
                      Dalete
                    </button>
                    <button
                      className="btn btn-outline-warning  my-2  w-100"
                      onClick={() => updateBtn(i, el)}
                      data-bs-toggle="modal"
                      data-bs-target="#exampleModal"
                    >
                      Update
                    </button>
                    <button
                      className="btn btn-outline-primary mb-2"
                      data-bs-toggle="modal"
                      data-bs-target="#exampleModal"
                      onClick={() => addAdmin(i)}
                    >
                      Create Admin
                    </button>
                    <div className="switch ms-4">
                      <input
                        id={i}
                        type="checkbox"
                        className="switch-input ms-1"
                      />
                      <label for={i} className="switch-label ">
                        Switch
                      </label>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
      <div
        class="modal fade  "
        style={{ width: "100%" }}
        id="exampleModal"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog ">
          <div class="modal-content p-3">
            <div class="modal-header">
              <button
                type="button"
                class="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={() => setBtn("")}
              ></button>
            </div>
            <div class="modal-body">
              {btn === "addOrg" ? (
                <AddOrganization onAddFunc={addStaff} />
              ) : btn === "add" ? (
                <AddAdmin />
              ) : (
                <UpdateOrganization onUpdateOrg={updateOrg} data={element} />
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
