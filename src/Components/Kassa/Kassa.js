import React, { useState } from "react";
import "../Sales/sale.css";

import ProductForm from "../Sales/ProductAddForm";
import jsonData from "../Sales/data.json";
import SideBar from "../SideBar/SideBar";

export default function Kassa() {
  const [isToday, setToday] = useState(true);
  const [isIncome, setIncome] = useState(false);
  const [isOutcome, setOutcome] = useState(false);

  const handlClick = (e) => {
    console.log(e.target.textContent);
    if (e.target.textContent == "Today's products") {
      setToday(true);
      setIncome(false);
      setOutcome(false);
      console.log(isToday, isIncome, isOutcome);
    } else if (e.target.textContent == "Outcome productList") {
      setOutcome(true);
      setIncome(false);
      setToday(false);
      console.log(isToday, isIncome, isOutcome);
    } else if (e.target.textContent == "Income ProductList") {
      setIncome(true);
      setOutcome(false);
      setToday(false);
      console.log(isToday, isIncome, isOutcome);
    }
  };

  return (
    <>
      <SideBar />
      <div className="container">
        <ul className="salemenu">
          <li className="pro">
            <a href="#" onClick={handlClick}>
              Today's products
            </a>
          </li>

          <li className="pro">
            <a href="#" onClick={handlClick}>
              Outcome productList
            </a>
          </li>

          <li className="pro">
            <a href="#" onClick={handlClick}>
              Income ProductList
            </a>
          </li>
        </ul>

        {isToday && <TodayProductList />}
        {isIncome && <IncomeProductList />}
        {isOutcome && <OutcomeProductList />}
      </div>
    </>
  );
}

const OutcomeProductList = () => {
  let arr = [1, 2, 3, 4, 5];
  return (
    <>
      <h1 className="text-xs-center text-center regtitle">Product List</h1>
      <div className="row">
        <div className="col-8 col-md-8 offset-2 d-flex">
          <div class="input-group searchbar">
            <input
              type="text"
              class="form-control searchinput"
              placeholder="Search by date "
              aria-label="Recipient's username"
              aria-describedby="basic-addon2"
            />
            <ion-icon name="search-outline" id="basic-addon2"></ion-icon>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-8 offset-2">
          <h4 className="text-center my-3">List History</h4>
          <UserInputProductList />
        </div>
      </div>
    </>
  );
};

const IncomeProductList = () => {
  return (
    <>
      <h1>Outcome product list here</h1>
    </>
  );
};

const TodayProductList = () => {
  return (
    <>
      <h1 className="text-xs-center text-center regtitle">
        Today's product list
      </h1>
      <div className="row ms-2">
        <div className="col-9 col-md-9 offset-2">
          <div className="accordion" id="accordionExample">
            <div className="accordion-item">
              <h2 className="accordion-header" id="headingOne">
                <button
                  className="accordion-button"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseOne"
                  aria-expanded="true"
                  aria-controls="collapseOane"
                >
                  <span>1</span>-User
                </button>
              </h2>
              <div
                id="collapseOne"
                className="accbody accordion-collapse collapse show"
                aria-labelledby="headingOne"
                data-bs-parent="#accordionExample"
              >
                <div className="accordion-body">
                  <UserInputProductList />
                </div>
              </div>
            </div>
            <div className="accordion-item">
              <h2 className="accordion-header" id="headingTwo">
                <button
                  className="accordion-button collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseTwo"
                  aria-expanded="false"
                  aria-controls="collapseTwo"
                >
                  <span>2</span>-User
                </button>
              </h2>
              <div
                id="collapseTwo"
                className="accbody accordion-collapse collapse"
                aria-labelledby="headingTwo"
                data-bs-parent="#accordionExample"
              >
                <div className="accordion-body">
                  <UserInputProductList />
                </div>
              </div>
            </div>
            <div className="accordion-item">
              <h2 className="accordion-header" id="headingThree">
                <button
                  className="accordion-button collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseThree"
                  aria-expanded="false"
                  aria-controls="collapseThree"
                >
                  <span>3</span>-User
                </button>
              </h2>
              <div
                id="collapseThree"
                className="accbody accordion-collapse collapse"
                aria-labelledby="headingThree"
                data-bs-parent="#accordionExample"
              >
                <div className="accordion-body">
                  <UserInputProductList />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const UserInputProductList = () => {
  const [pay, setPay] = useState(false);

  const [productData, setProductData] = useState(jsonData);
  // commentbox
  const [isOpen, setOpen] = useState(false);

  const handleOpenComment = (e) => {
    e.preventDefault();
    setOpen(true);
  };
  const handleCloseComment = (e) => {
    e.preventDefault();
    setOpen(false);
  };
  const onPay = () => {
    console.log("pay");
    setPay(!pay);
  };

  const tableRows = productData.map((item, index) => {
    let total = item.price * item.number;

    return (
      <tr key={index}>
        <td className="border border-collapse px-2">{item.id}</td>
        <td className="border border-collapse px-2">{item.name}</td>
        <td className="border border-collapse px-2">{item.color}</td>
        <td className="border border-collapse px-2">{item.type}</td>
        <td className="border border-collapse px-2">{item.date}</td>
        <td className="border border-collapse px-2">{item.price}</td>
        <td className="border border-collapse px-2">{item.number}</td>
        <td className="border border-collapse px-2">{total}</td>
        <td className="border border-collapse px-2">
          <button className="btn btn-outline-success m-1">
            {item.completed ? `completed` : `inproggress`}
          </button>
        </td>
        <td className="border border-collapse px-2 comenttd">
          <button
            type="button"
            class="btn btn-outline-primary m-1"
            onClick={handleOpenComment}
          >
            comment...
          </button>
          {isOpen ? (
            <>
              <div className="commentbox">
                {item.comment}
                <button className="commentClose" onClick={handleCloseComment}>
                  close
                </button>
              </div>
            </>
          ) : (
            ""
          )}
        </td>
      </tr>
    );
  });

  const addRows = (data) => {
    const totalProducts = productData.length;
    data.id = totalProducts + 1;

    const updateProductData = [...productData];
    updateProductData.push(data);
    setProductData(updateProductData);
  };

  return (
    <>
      <table className="table hover border usertable">
        <thead>
          <tr>
            <th className="border border-collapse p-3">T/r</th>
            <th className="border border-collapse p-3">Product name</th>
            <th className="border border-collapse p-3">Color</th>
            <th className="border border-collapse p-3">Type</th>
            <th className="border border-collapse p-3">Date</th>
            <th className="border border-collapse p-3">Price</th>
            <th className="border border-collapse p-3">Number</th>
            <th className="border border-collapse p-3">Total Price</th>
            <th className="border border-collapse p-3">Status</th>
            <th className="border border-collapse p-3">Comment</th>
          </tr>
        </thead>
        <tbody>{tableRows}</tbody>
      </table>
      {/* foot buttons kassa and add product */}
      <form className="foottable">
        <button
          className="btn btn-danger"
          style={
            pay ? { background: "btn-success" } : { background: "btn-danger" }
          }
          onClick={onPay}
        >
          PAY
        </button>
      </form>
    </>
  );
};
