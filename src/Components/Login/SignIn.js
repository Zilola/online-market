import React, { useState } from "react";
import { Link } from "react-router-dom";
import useJwt from "../../auth/jwt/useJwt";
import axios from "axios";

export default function SignIn() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const onUserchange = (e) => {
    if (e.target.name === "user") {
      setUsername(e.target.value);
    } else setPassword(e.target.value);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    let data = { username, password };
    if (Object.values(data).every((field) => field.length > 0)) {
      console.log(data);
      // axios
      //   .post("https://wholesale-system.herokuapp.com/api/v1/auth/login", data)
      //   .then((response) => {
      //     console.log(response);
      //   });

      axios
        .post("https://wholesale-system.herokuapp.com/api/v1/auth/login", data)
        .then((response) => console.log(response));
      // useJwt
      //   .login()
      //   .then((res) => {
      //     console.log(res);
      //     // const data = {
      //     //   ...res.data.response.userData,
      //     //   accessToken: res.data.response.access_token,
      //     //   refreshToken: res.data.response.refreshToken,
      //     // };
      //     // console.log(data);
      //     // dispatch(handleLogin(data));
      //     // ability.update(res.data.response.userData.ability);
      //     // history.push(getHomeRouteForLoggedInUser(data.role));
      //     // toast.success(
      //     //   <ToastContent
      //     //     name={data.fullName || data.username || "John Doe"}
      //     //     role={data.role || "admin"}
      //     //   />,
      //     //   {
      //     //     icon: false,
      //     //     transition: Slide,
      //     //     hideProgressBar: true,
      //     //     autoClose: 2000,
      //     //   }
      //     // );
      //   })
      //   .catch((err) => console.log(err));
    }
    // else {
    //   for (const key in data) {
    //     if (data[key].length === 0) {
    //       setError(key, {
    //         type: "manual",
    //       });
    //     }
    //   }
    // }
  };
  return (
    <React.Fragment>
      <h1 className="text-xs-center text-center regtitle">Log In</h1>
      <p className="cetrtext">
        <Link to="/signIn" className="subtitletext">
          Need an account?
        </Link>
      </p>
      <form className="loginform">
        <fieldset>
          <input
            type="text"
            name="user"
            value={username}
            className="LoginInput"
            placeholder="Username"
            onChange={onUserchange}
          />
        </fieldset>

        <fieldset>
          <input
            type="password"
            value={password}
            className="LoginInput"
            placeholder="Password"
            onChange={onUserchange}
          />
        </fieldset>

        <fieldset>
          <button type="submit" className="SignBtn" onClick={onSubmit}>
            {/* <button type="submit" className="SignBtn"> */}
            Login
          </button>
        </fieldset>
      </form>
    </React.Fragment>
  );
}
