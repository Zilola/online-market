import React, { useEffect, useState } from "react";
import "./sale.css";
import SideBar from "../SideBar/SideBar";
import ProductForm from "./ProductAddForm";
import jsonData from "./data.json";

export default function Sales() {
  const [isToday, setToday] = useState(true);
  const [isIncome, setIncome] = useState(false);
  const [isOutcome, setOutcome] = useState(false);

  const handlClick = (e) => {
    console.log(e.target.textContent);
    if (e.target.textContent == "Today's products") {
      setToday(true);
      setIncome(false);
      setOutcome(false);
      console.log(isToday, isIncome, isOutcome);
    } else if (e.target.textContent == "Outcome productList") {
      setOutcome(true);
      setIncome(false);
      setToday(false);
      console.log(isToday, isIncome, isOutcome);
    } else if (e.target.textContent == "Income ProductList") {
      setIncome(true);
      setOutcome(false);
      setToday(false);
      console.log(isToday, isIncome, isOutcome);
    }
  };

  return (
    <>
      {/* <SideBar /> */}
      <div className="container">
        {/* <ul className="salemenu">
          <li className="pro">
            <a href="#" onClick={handlClick}>
              Today's products
            </a>
          </li>

          <li className="pro">
            <a href="#" onClick={handlClick}>
              Outcome productList
            </a>
          </li>

          <li className="pro">
            <a href="#" onClick={handlClick}>
              Income ProductList
            </a>
          </li>
        </ul> */}

        {isToday && <TodayProductList />}
        {isIncome && <IncomeProductList />}
        {isOutcome && <OutcomeProductList />}
      </div>
    </>
  );
}

export const OutcomeProductList = () => {
  const data = [
    {
      user: "guli",
      item_count: 0,
      model: "string",
      color: "string",
      firmId: 0,
      default_price: 0,
      sectorId: 0,
    },
  ];
  return (
    <>
    <SideBar/>
      <h1 className="text-xs-center text-center regtitle">Product List</h1>
      <div className="row">
        <div className="col-8 col-md-8 offset-2 d-flex">
          <div class="input-group searchbar">
            <input
              type="text"
              class="form-control searchinput"
              placeholder="Search by date "
              aria-label="Recipient's username"
              aria-describedby="basic-addon2"
            />
            <ion-icon name="search-outline" id="basic-addon2"></ion-icon>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-8 offset-2">
          <h4 className="text-center my-3">List History</h4>
          <table className="table hover border usertable">
            <thead>
              <tr>
                <th className="border border-collapse p-3">T/r</th>
                <th className="border border-collapse p-3">User</th>
                <th className="border border-collapse p-3">firm id</th>
                <th className="border border-collapse p-3">sector id</th>
                <th className="border border-collapse p-3">Color</th>
                <th className="border border-collapse p-3">Model</th>
                <th className="border border-collapse p-3">Date</th>
                <th className="border border-collapse p-3">Price</th>
                <th className="border border-collapse p-3">Number</th>
                <th className="border border-collapse p-3">Total Price</th>
                <th className="border border-collapse p-3">Status</th>
                <th className="border border-collapse p-3">Comment</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </>
  );
};

export const IncomeProductList = () => {
  return (
    <>
    <SideBar/>
      <h1 className="text-xs-center text-center regtitle">Product List</h1>
      <div className="row">
        <div className="col-8 col-md-8 offset-2 d-flex">
          <div class="input-group searchbar">
            <input
              type="text"
              class="form-control searchinput"
              placeholder="Search by date "
              aria-label="Recipient's username"
              aria-describedby="basic-addon2"
            />
            <ion-icon name="search-outline" id="basic-addon2"></ion-icon>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-8 offset-2">
          <h4 className="text-center my-3">The Rest Product List</h4>
          <table className="table hover border usertable">
            <thead>
              <tr>
                <th className="border border-collapse p-3">T/r</th>

                <th className="border border-collapse p-3">firm id</th>
                <th className="border border-collapse p-3">sector id</th>
                <th className="border border-collapse p-3">Color</th>
                <th className="border border-collapse p-3">Model</th>
                <th className="border border-collapse p-3">Price</th>
                <th className="border border-collapse p-3">Count</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </>
  );
};

export const TodayProductList = () => {
  const [openclient, setOpenClient] = useState(false);
  const [ClientName, setClientName] = useState("");
  const [Clientlist, setClientlist] = useState([]);
  const [AllClient, setAllClient] = useState([]);

  const handleOpenClient = (e) => {
    e.preventDefault();
    setOpenClient(true);
  };

  const handleChaneClient = (e) => {
    e.preventDefault();
    setClientName(e.target.value);
  };

  const handleCloseClient = (e) => {
    e.preventDefault();
    if (ClientName) {
      setClientlist([
        ...Clientlist,
        { name: ClientName, id: Math.random().toString(36) },
      ]);
      setClientName("");
      setTimeout(() => setOpenClient(false), 1000);
      console.log("clikentlist", Clientlist);
    }

    // setClientName()
  };

  return (
    <>
    <SideBar/>
      <h1 className="text-xs-center text-center regtitle">
        Today's product list
      </h1>
      <div className="row ms-2">
        <div className="col-9 col-md-9 offset-2 toprow">
          <button
            className="btn btn-success text-center mb-3"
            onClick={handleOpenClient}
          >
            Add Client
          </button>
          {/* klient qo'shish uchun forma */}
          {openclient && (
            <>
              <div className="clientmodal">
                <h5 class="text-white display-6 text-center">
                  Client Information
                </h5>
                <p className="text-center text-white pb-5">
                  Client information details:
                </p>
                <form className="productform">
                  <fieldset>
                    <label className="text-white labelclient">
                      Client name{" "}
                    </label>
                    <br />
                    <input
                      type="text"
                      placeholder="Client Name"
                      className="productadd w-100 m-0"
                      value={ClientName}
                      onChange={handleChaneClient}
                    />
                  </fieldset>
                  <fieldset>
                    <input
                      type="submit"
                      className="clientaddBtn btn btn-info text-white mt-3"
                      value="addClient"
                      onClick={handleCloseClient}
                    />
                  </fieldset>

                  {/* <fieldset>
                  <label>default price </label>
                  <input
                    type="number"
                    placeholder="default price"
                    className="productadd"
                    value={price}
                    onChange={changePrice}
                  />
                </fieldset> */}
                </form>
              </div>
            </>
          )}

          {/* accardion of users inner each accardion has table of user product orders */}

          {Clientlist.map((user, index) => {
            return (
              <>
                <div className="card mt-3" key={index}>
                  <div className="card-body">
                    <h2 className="card-title">
                      <span>{user.id}</span>
                      {user.name}
                    </h2>

                    <UserInputProductList userid={user.id} />
                  </div>
                </div>
              </>
            );
          })}
        </div>
      </div>
    </>
  );
};

const UserInputProductList = (props) => {
  const [productData, setProductData] = useState(jsonData);
  const whichUser = props.userid;
  console.log("whichUser", whichUser);
  // commentbox
  const [isOpen, setOpen] = useState(false);

  const handleOpenComment = (e) => {
    e.preventDefault();
    setOpen(true);
  };
  const handleCloseComment = (e) => {
    e.preventDefault();
    setOpen(false);
  };

  // data of table:

  // localStorage.setItem("products", JSON.stringify(productData));
  let Summa = 0;
  const tableRows = productData.map((item, index) => {
    let total = item.default_price * item.item_count;
    Summa += total;
    return (
      <tr key={index}>
        <td className="border border-collapse px-2">{item.id}</td>
        <td className="border border-collapse px-2">{item.model}</td>
        <td className="border border-collapse px-2">{item.color}</td>
        <td className="border border-collapse px-2">{item.firmId}</td>
        <td className="border border-collapse px-2">{item.sectorId}</td>
        <td className="border border-collapse px-2">{item.default_price}</td>
        <td className="border border-collapse px-2">{item.item_count}</td>
        <td className="border border-collapse px-2">{total}</td>
        <td className="border border-collapse px-2">
          {new Date()
            .toISOString()
            .replace(/T.*/, "")
            .split("-")
            .reverse()
            .join("-")}
        </td>
        <td className="border border-collapse px-2">
          <button className="btn btn-outline-success m-1">
            {item.completed ? `completed` : `inproggress`}
          </button>
        </td>
      </tr>
    );
  });

  const addRows = (data) => {
    const totalProducts = productData.length;
    data.id = totalProducts + 1;

    const updateProductData = [...productData];
    updateProductData.push(data);
    setProductData(updateProductData);
  };

  return (
    <>
      <table className="table hover border usertable">
        <thead>
          <tr>
            <th className="border border-collapse p-3">T/r</th>
            <th className="border border-collapse p-3">Model</th>
            <th className="border border-collapse p-3">Color</th>
            <th className="border border-collapse p-3">Firm Id</th>
            <th className="border border-collapse p-3">Sector Id</th>
            <th className="border border-collapse p-3">Default Price</th>
            <th className="border border-collapse p-3">Item Count</th>
            <th className="border border-collapse p-3">Total Price</th>
            <th className="border border-collapse p-3">Date</th>
            <th className="border border-collapse p-3">Status</th>
          </tr>
        </thead>
        <tbody>{tableRows}</tbody>
      </table>
      {/* foot buttons kassa and add product */}
      <form className="foottable">
        <spam className="allSumma me-2">Sum:{Summa}</spam>
        <button className="btn btn-warning">Kassa</button>
        <button
          type="button"
          className="btn btn-success ms-2"
          data-bs-toggle="modal"
          data-bs-target="#exampleModal"
          // disabled={openModal}
        >
          Add product
        </button>
        <button
          type="button"
          class="btn btn-outline-primary ms-2"
          onClick={handleOpenComment}
        >
          comment...
        </button>
        {isOpen ? (
          <>
            <div className="commentbox">
              `user comment will be here user comment will be her user comment
              will be her user comment will be hervuser comment will be her `
              <button className="commentClose" onClick={handleCloseComment}>
                close
              </button>
            </div>
          </>
        ) : (
          ""
        )}

        {/* total opshi summa */}
      </form>

      {/* start modal */}
      <ProductForm func={addRows} />
      {/* end modal */}
    </>
  );
};
