import React, { useState } from "react";

function ProductForm(props) {
  const [model, setNameModel] = useState("");
  const [default_price, setPrice] = useState();
  const [color, setColor] = useState("");
  const [item_count, setNumber] = useState();
  const [firmId, setFirmId] = useState("");
  const [date, setDate] = useState("");
  const [sectorId, setSectorId] = useState("");
  const [completed, setStatus]=useState(false);

  // functions for take input values;

  const changeNameModel = (event) => {
    setNameModel(event.target.value);
  };

  const changePrice = (event) => {
    setPrice(event.target.value);
  };

  const changeColor = (event) => {
    setColor(event.target.value);
  };

  const changeNumber = (event) => {
    setNumber(event.target.value);
  };

  const changeFirmId = (event) => {
    setFirmId(event.target.value);
  };

  const changeDate = (event) => {
    setDate(event.target.value);
  };

  const changeSectorId = (event) => {
    setSectorId(event.target.value);
  };
  const changeStatus=(event)=>{
      setStatus(event.target.value);
  }

  //   button to transef input values to a table:
  const transferValue = (event) => {
    event.preventDefault();
    const val = {
      model,
      color,
      firmId,
      sectorId,
      default_price,
      item_count,
      completed
    };
    props.func(val);
    clearState();
  };

  const clearState = () => {
    setNameModel("");
    setPrice("");
    setColor("");
    setNumber("");
    setFirmId("");
    setDate("");
    setSectorId("");
  };

  return (
    <>
      <div
        class="modal fade "
        id="exampleModal"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog ">
          <div class="modal-content p-3">
            <div class="modal-header">
              <button
                type="button"
                class="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div class="modal-body">
              <h5 class="modal-title text-center" id="exampleModalLabel">
                Product Information
              </h5>
              <p className="text-center">
                Information to help define a product.
              </p>
              <form className="productform">
                <fieldset>
                  <label>product model </label>
                  <input
                    type="text"
                    placeholder="product model"
                    className="productadd"
                    value={model}
                    onChange={changeNameModel}
                  />
                </fieldset>
                <fieldset>
                  <label>default price </label>
                  <input
                    type="number"
                    placeholder="default price"
                    className="productadd"
                    value={default_price}
                    onChange={changePrice}
                  />
                </fieldset>
                <fieldset>
                  <label>color </label>
                  <input
                    type="text"
                    placeholder="color"
                    className="productadd"
                    value={color}
                    onChange={changeColor}
                  />
                </fieldset>
                <fieldset>
                  <label>number</label>
                  <input
                    type="number"
                    placeholder="Item count"
                    className="productadd"
                    value={item_count}
                    onChange={changeNumber}
                  />
                </fieldset>
                <fieldset>
                  <label>Firm Id</label>
                  <input
                    type="text"
                    placeholder="Firm Id"
                    className="productadd"
                    value={firmId}
                    onChange={changeFirmId}
                  />
                </fieldset>

                <fieldset>
                  <label>Order date</label>
                  <input
                    type="date"
                    placeholder="date"
                    className="productadd"
                    value={date}
                    onChange={changeDate}
                  />
                </fieldset>

                <fieldset>
                  <label>Status</label>
                  <input
                    type="boolean"
                    placeholder="product status"
                    className="productadd"
                    onChange={changeStatus}
                    value={completed}
                    disabled={!completed}
                  />
                </fieldset>

                <fieldset>
                  <label>Sector Id</label>
                  <input
                    type="number"
                    placeholder="sector ID"
                    className="productadd"
                    onChange={changeSectorId}
                    value={sectorId}
                    // disabled={!completed}
                  />
                </fieldset>

                {/* <fieldset>
                  <label>Comment:</label>
                  <textarea
                    value={comment}
                    onChange={changeComment}
                    className="productadd"
                  ></textarea>
                </fieldset> */}
              </form>
            </div>
            <div class="modal-footer">
              <b className="text-rigth d-block"></b>
              <br />
            </div>
            <button
              type="button"
              class="btn text-white modalBtn w-50"
              style={{ margin: "0 auto" }}
              data-bs-dismiss="modal"
              onClick={transferValue}
            >
              Add product
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default ProductForm;
