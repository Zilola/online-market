import React, { useState } from "react";
import { Link } from "react-router-dom";

export default function SideBar() {
  const [showNav, setshowNav] = useState(true);
  const [role, setRole] = useState("ADMIN");

  const toggleBtn = () => {
    setshowNav(!showNav);
  };
  return (
    <div>
      <header className="header" id="header">
        <div className="header_toggle">
          <i
            className="bx bx-menu"
            id="header-toggle"
            onClick={toggleBtn}
            style={showNav ? { marginLeft: "160px" } : { marginLeft: "0" }}
          ></i>
        </div>

        <div className="d-flex align-items-center">
          <p className=" mb-0">User name</p>
          <div className="header_img m-4">
            <img src="https://i.imgur.com/hczKIze.jpg" alt="" />
          </div>
        </div>
      </header>
      <div
        className="l-navbar"
        id="nav-bar"
        style={showNav ? { width: "250px" } : { width: "75px" }}
      >
        <nav className="nav">
          <div>
            <a href="#" className="nav_logo">
              Logo
            </a>
            <div className="nav_list">
              <a href="#">
                <Link
                  to={role === "ADMIN" ? "/admin" : "/sales"}
                  className="nav_link"
                >
                  <i className="bx bx-grid-alt nav_icon"></i>
                  <span className="nav_name">Dashboard</span>
                </Link>
              </a>

              <a
                href="#"
                style={
                  role === "ADMIN" ? { display: "block" } : { display: "none" }
                }
              >
                <Link to="/admin/userList" className="nav_link">
                  <i className="bx bx-user nav_icon"></i>
                  <span className="nav_name">User Management</span>
                </Link>
              </a>
              <a
                href="#"
                style={
                  role === "ADMIN" ? { display: "block" } : { display: "none" }
                }
              >
                <Link to="/admin/marketList" className="nav_link">
                  <i className="bx bx-message-square-detail nav_icon"></i>
                  <span className="nav_name">Market Management</span>
                </Link>
              </a>
              <a
                href="#"
                style={
                  role === "ADMIN" ? { display: "block" } : { display: "none" }
                }
              >
                <Link to="/companyList" className="nav_link">
                  <i className="bx bx-bookmark nav_icon"></i>
                  <span className="nav_name">Company Management</span>
                </Link>
              </a>
              <a
                href="#"
                style={
                  role === "ADMIN" ? { display: "block" } : { display: "none" }
                }
              >
                <Link to="/sales" className="nav_link">
                  <i className="bx bx-bar-chart-alt-2 nav_icon"></i>

                  <span className="nav_name">Sales</span>
                </Link>
              </a>
              <a
                href="#"
                style={
                  role === "ADMIN" ? { display: "block" } : { display: "none" }
                }
              >
                <Link to="/cashier" className="nav_link">
                  <i class="bi bi-coin nav_icon"></i>

                  <span className="nav_name">Cashier</span>
                </Link>
              </a>
              <a
                href="#"
                style={
                  role === "ADMIN" ? { display: "block" } : { display: "none" }
                }
              >
                <Link to="/wareHouse" className="nav_link">
                  <i className="bx bx-folder nav_icon"></i>

                  <span className="nav_name">WareHouse</span>
                </Link>
              </a>
              {/* // Start  Sales SideBar */}
              <a
                href="#"
                style={
                  role === "SALES" ? { display: "block" } : { display: "none" }
                }
              >
                <Link to="/sales/today" className="nav_link">
                  <i class="bi bi-graph-up-arrow nav_icon"></i>

                  <span className="nav_name">Today`s product</span>
                </Link>
              </a>
              <a
                href="#"
                style={
                  role === "SALES" ? { display: "block" } : { display: "none" }
                }
              >
                <Link to="/sales/outcome" className="nav_link">
                  <i class="bi bi-calendar-week nav_icon"></i>
                  <span className="nav_name">Outcome product</span>
                </Link>
              </a>
              <a
                href="#"
                style={
                  role === "SALES" ? { display: "block" } : { display: "none" }
                }
              >
                <Link to="/sales/income" className="nav_link">
                  <i className="bx bx-bar-chart-alt-2 nav_icon"></i>

                  <span className="nav_name">Income product</span>
                </Link>
              </a>

              {/* End sales Sidebar */}
              <a href="#">
                <Link to="/setting" className="nav_link">
                  <i class="bi bi-gear nav_icon "></i>
                  <span className="nav_name">Setting</span>
                </Link>
              </a>
            </div>
          </div>

          <a href="#">
            <Link to="/" className="nav_link">
              <i className="bx bx-log-out nav_icon"></i>
              <span className="nav_name">Long out</span>
            </Link>
          </a>
        </nav>
      </div>
    </div>
  );
}

document.addEventListener("DOMContentLoaded", function (event) {
  // const showNavbar = (toggleId, navId, bodyId, headerId) => {
  //   // const toggle = document.getElementById(toggleId),
  //   //   nav = document.getElementById(navId),
  //   //   bodypd = document.getElementById(bodyId),
  //   //   headerpd = document.getElementById(headerId);

  //   // Validate that all variables exist
  //   // if (toggle && nav && bodypd && headerpd) {
  //   //   toggle.addEventListener("click", () => {
  //   //     // show navbar
  //   //     nav.classList.toggle("show");
  //   //     // change icon
  //   //     toggle.classList.toggle("bx-x");
  //   //     // add padding to body
  //   //     bodypd.classList.toggle("body-pd");
  //   //     // add padding to header
  //   //     headerpd.classList.toggle("body-pd");
  //   //   });
  //   // }
  // };

  // showNavbar("header-toggle", "nav-bar", "body-pd", "header");

  /*===== LINK ACTIVE =====*/
  const linkColor = document.querySelectorAll(".nav_link");

  function colorLink() {
    if (linkColor) {
      linkColor.forEach((l) => l.classList.remove("active"));
      this.classList.add("active");
    }
  }
  linkColor.forEach((l) => l.addEventListener("click", colorLink));
});
