import React, { useState } from "react";
class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      /* Awesome State Not Yet Used */
    };
  }

  render() {
    return (
      <div
        className={`sidebar-menu${
          this.props.isMenuOpen === true ? " open" : ""
        }`}
      >
        <button
          type="button"
          className="button small float-right"
          onClick={this.props.onMenuToggle}
        >
          Toggle Menu
        </button>
        <ul className="vertical menu">
          <li>
            <a>Menu Item</a>
          </li>
          <li>
            <a>Menu Item</a>
          </li>
          <li>
            <a>Menu Item</a>
          </li>
          <li>
            <a>Menu Item</a>
          </li>
          <li>
            <a>Menu Item</a>
          </li>
        </ul>
      </div>
    );
  }
}

export default Menu;
